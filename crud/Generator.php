<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace bdb\giiant\crud;

use yii\gii\CodeFile;
use yii\helpers\Inflector;
use Yii;
use yii\helpers\StringHelper;
use yii\db\Schema;

/**
 * This generator generates an extended version of CRUDs.
 * @author Tobais Munk <schmunk@usrbin.de>
 * @since 1.0
 */
class Generator extends \schmunk42\giiant\crud\Generator
{

    public $isPivot = false;

    public function getPivot()
    {
        $obj = [];
        foreach ($this->getModelRelations($this->modelClass, ['pivot']) as $key => $value) {
            $this->isPivot = true;
            $pivot = lcfirst($key);
            $pivotClass = new $value->modelClass;
            $pivotSchema = $pivotClass->getTableSchema();
            $many = lcfirst($this->getManyMany($pivotSchema->foreignKeys)) . 's';
            $spinner = false;
            $text = false;
            $select = false;

            foreach ($pivotSchema->columns as $key => $value) {
                if($column->comment != NULL) {
                    $comment = json_decode($column->comment, true);
                    switch ($comment['formType']) {
                        case 'FIELD_DROPDOWN':
                            $select = $value->name;
                            break;

                        case 'FIELD_SPINNER':
                            $spinner = $value->name;
                            break;

                        case 'FIELD_TEXT_AREA':
                            $text = $value->name;
                            break;
                    }
                }
            }

            $obj[] = [
                'manyMany' => $many,
                'pivot' => $pivot,
                'spinner' => $spinner,
                'text' => $text,
                'select' => $select
            ];
        }
        return $obj;
    }


    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {

            $filterTypeDef = null;

            if (!empty($column->comment)) {                       
                       $commentMeta = json_decode($column->comment);
                       $filterTypeDef = $commentMeta->filterType;
                }

            if ($filterTypeDef != 'FIELD_BLOCK' && $filterTypeDef != 'FILTER_BLOCK' && !$column->isPrimaryKey && substr($column->name, -2) != 'Id')
            {
                switch ($column->type) {
                    case Schema::TYPE_SMALLINT:
                    case Schema::TYPE_INTEGER:
                    case Schema::TYPE_BIGINT:
                        $types['integer'][] = $column->name;
                        break;
                    case Schema::TYPE_BOOLEAN:
                        $types['boolean'][] = $column->name;
                        break;
                    case Schema::TYPE_FLOAT:
                    case Schema::TYPE_DOUBLE:
                    case Schema::TYPE_DECIMAL:
                    case Schema::TYPE_MONEY:
                        $types['number'][] = $column->name;
                        break;
                    case Schema::TYPE_DATE:
                    case Schema::TYPE_TIME:
                    case Schema::TYPE_DATETIME:
                    case Schema::TYPE_TIMESTAMP:
                    default:
                        $types['safe'][] = $column->name;
                        break;
                }
            }
        }

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }

        return $rules;
    }


    public function getManyMany($FKs)
    {
        $modelName = $this->getTableSchema()->name;

        array_walk($FKs, function (&$item, $key) {
            $item = array_shift($item);
        });

        $manyMany = $FKs[0] == $modelName ? $FKs[1] : $FKs[0];

        return $manyMany;
    }

    public function setField($column, $FKs, $fkClass, $tableName)
    {
        $USE = "";
        $HTML = "";

        $fkTable = $FKs[$column->name];
   
        if($column->comment != NULL) {
            $comment = json_decode($column->comment, true);

            switch ($comment['formType']) {

                case 'FIELD_SPIN_INCREMENT':

                    $USE = "use kartik\widgets\TouchSpin;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(TouchSpin::classname(), [
                                'options' => [
                                    'placeholder' => Yii::t('app', 'FIELD_SPIN_INCREMENT'),
                                ],
                                'pluginOptions' => [
                                    'verticalbuttons' => true,
                                    'buttonup_class' => 'btn btn-gray', 
                                    'buttondown_class' => 'btn btn-gray'
                                ]
                        ]); ?>
                        ";

                    break;

                case 'FIELD_SPIN_ADD':

                    $USE = "use kartik\widgets\TouchSpin;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(TouchSpin::classname(), [
                                'options' => [
                                    'placeholder' => Yii::t('app', 'FIELD_SPIN_ADD'),

                                ],
                                'pluginOptions' => [
                                    'buttonup_class' => 'btn btn-gray', 
                                    'buttondown_class' => 'btn btn-gray', 
                                    'buttonup_txt' => '+', 
                                    'buttondown_txt' => '-'
                                ]
                        ]); ?>
                        ";

                    break;

                case 'FIELD_SWITCH':

                    $USE = "use kartik\widgets\SwitchInput;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->checkbox(array('class' => 'iswitch')) ?>
                        ";

                    break;


                case 'FIELD_CHECKBOX':

                    $USE = "use kartik\checkbox\CheckboxX;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(CheckboxX::classname(), ['pluginOptions'=>['size'=>'xs', 'threeState'=>false]]) ?>
                        ";

                    break;

                case 'FIELD_DROPDOWN':

                    if (!is_null($fkTable)) {
                        $_temp = new $fkClass;

                        $USE = "use kartik\widgets\Select2;\n";
                        $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(Select2::classname(), [
                                'language' => 'pt-br',
                                'data' => \yii\helpers\ArrayHelper::map($fkClass::find()->all(),'id','{$_temp->attributes()[1]}'),
                                'options' => ['placeholder' => Yii::t('app', 'FIELD_DROPDOWN')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                        ]) ?>
                        ";
                    }

                    break;


                case 'FIELD_DEPENDENT_DROPDOWN':

                    if (!is_null($fkTable)) {
                        $_temp = new $fkClass;

                        $USE = "use kartik\widgets\DepDrop;\n";
                        $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(DepDrop::classname(), [
                                'type'=>DepDrop::TYPE_SELECT2,
                                'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                'data' => \yii\helpers\ArrayHelper::map($fkClass::find()->where(['" . $comment['formOptions']['depends'] . "' => \$model->" . $comment['formOptions']['depends'] . " ])->all(),'id','{$_temp->attributes()[1]}'),
                                'options' => ['placeholder' => Yii::t('app', 'FIELD_DEPENDENT_DROPDOWN')],
                                'pluginOptions'=>[
                                    'depends'=>['" . strtolower($tableName . '-' . $comment['formOptions']['depends']) . "'],
                                    'url' => Url::to(['/" . strtolower($tableName) . "/subcat" . strtolower($fkTable) . "'])
                                ]
                             ]);
                        ?>
                        ";
                    }

                    break;


                /*
                case 'FIELD_TYPEAHEAD':

                     if(!is_null($fkTable))
                     {
                         $_temp = new $fkClass;

                         $USE = "use kartik\widgets\Typeahead;\n";
                         $HTML =  "
                         <?php // http://demos.krajee.com/widget-details/typeahead ?>
                         <!-- REVIEW -->
                         <?= \$form->field(\$model, '{$column->name}')->widget(Typeahead::classname(), [
                                 'options' => ['placeholder' => Yii::t('app', 'Filter as you type ...')],
                                 'dataset' => [
                                     [
                                         'local' =>  \yii\helpers\ArrayHelper::map(backend\models\State::find()->all(),'id','name'),
                                         'limit' => 10
                                     ]
                                 ],

                         ]) ?>
                         ";
                     }

                 break;
                 */

                case 'FIELD_REVIEW':

                    $USE = "use kartik\widgets\StarRating;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(StarRating::classname(), ['pluginOptions'=>['size'=>'xs', 'showCaption'=>false, 'glyphicon'=>false]]) ?>
                        ";

                    break;

                case 'FIELD_MONEY':

                    $USE = "use kartik\money\MaskMoney;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(MaskMoney::classname(), ['options' => ['maxlength' => 20]]); ?> ";

                    break;


                case 'FIELD_ENUM_RADIO':
                    $USE = "use bdb\input\Enum;\n";
                    $HTML = "<?= \$form->field(\$model, '{$column->name}')->widget(Enum::classname(), ['type' => 'RADIO']) ?>";

                    break;


                case 'FIELD_ENUM_DROPDOWN':
                    $USE = "use bdb\input\Enum;\n";
                    $HTML = "<?= \$form->field(\$model, '{$column->name}')->widget(Enum::classname(), ['type' => 'DROPDOWN']) ?>";

                    break;


                case 'FIELD_TEXT_INPUT':
                case 'FIELD_EMAIL':

                    //$USE = "use kartik\widgets\TouchSpin;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->textInput(['maxlength' => 255]) ?>
                        ";

                    break;

                case 'FIELD_TEXT_AREA':

                    //$USE = "use kartik\widgets\TouchSpin;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->textarea(['rows' => 6]) ?>
                        ";

                    break;

                case 'FIELD_TEXT_EDITOR':

                    $USE = "use kartik\markdown\MarkdownEditor;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(MarkdownEditor::classname(), [
                                'encodeLabels' => false,
                                'showExport' => false,
                                'showPreview' => true,
                                'options'=>[

                                    
                                ],
                        ]) ?>
                        ";

                    break;

                case 'FIELD_DATE':

                    $USE = "use kartik\datecontrol\DateControl;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                        ]) ?>";

                    break;

                case 'FIELD_TIME':

                    $USE = "use kartik\datecontrol\DateControl;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_TIME,
                        ]) ?>
                        ";

                    break;

                case 'FIELD_DATE_TIME':

                    $USE = "use kartik\datecontrol\DateControl;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(DateControl::classname(), [
                            'type' => DateControl::FORMAT_DATETIME,
                        ]) ?>
                        ";

                    break;


                case 'FIELD_IMAGE':

                    $USE = "use kartik\widgets\FileInput;\n";
                    $HTML = "
                        <?php

                        echo Html::img(\$model->getImageUrl('{$column->name}'), [
                            'class'=>'img-thumbnail', 
                        ]);

                        echo \$form->field(\$model, '{$column->name}')->widget(FileInput::classname(), [
                                'options'=>[
                                    'accept'=>'image/*',
                                    'maxlength' => 255,
                                    'multiple' => false,
                                ],
                                'pluginOptions'=>[
                                    'previewFileType' => 'image',
                                    'showUpload' => false,
                                    'showRemove' => false,
                                    'browseClass' => 'btn btn-gray',
                                    'browseLabel' => Yii::t('app', 'FIELD_IMAGE'),
                                 ],
                        ]) ?>
                        ";

                    break;


                case 'FIELD_FILE':

                    $USE = "use kartik\widgets\FileInput;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(FileInput::classname(), [
                                'options'=>[
                                    'maxlength' => 255,
                                    'multiple' => false,
                                ],
                                'pluginOptions'=>[
                                    'previewFileType' => 'image',
                                    'showPreview' => false,
                                    'showUpload' => false,
                                    'showRemove' => false,
                                    'browseClass' => 'btn btn-gray',
                                    'browseLabel' => Yii::t('app', 'FIELD_FILE'),
                                ],
                        ]) ?>
                        ";

                    break;

                case 'FIELD_PASSWORD':

                    //$USE = "use kartik\widgets\TouchSpin;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->passwordInput(['maxlength' => 60, 'placeholder' => Yii::t('app', 'FIELD_PASSWORD')]) ?>
                        ";

                    break;


                case 'FIELD_PHONE':

                    $USE = "use bdb\input\Masked;\n";
                    $HTML = "
                        <?php // ___URL___ ?>
                        <!-- PHONE -->
                        <?= \$form->field(\$model, '{$column->name}')->widget(Masked::classname(), [
                                'options'=>[
                                    'maxlength' => 14,
                                    'class' => 'form-control',
                                    'placeholder' => '(99) 9999-9999',
                                    ],
                                'addon' => ['prepend' => ['content'=>'<i class=\"glyphicon glyphicon-phone\"></i>']],
                                'mask' => '(99) 9999-9999',
                                'clientOptions' => ['clearIncomplete' => true]
                        ]) ?>
                        ";

                    break;

                case 'FIELD_MOBILE':

                    $USE = "use bdb\input\Masked;\n";
                    $HTML = "
                        <?php // ___URL___ ?>
                        <!-- MOBILE -->
                        <?= \$form->field(\$model, '{$column->name}')->widget(Masked::classname(), [
                                'options'=>[
                                    'maxlength' => 15,
                                    'class' => 'form-control',
                                    'placeholder' => '(99) 99999-9999',
                                    ],
                                'addon' => ['prepend' => ['content'=>'<i class=\"glyphicon glyphicon-phone\"></i>']],
                                'mask' => ['(99) 9999-9999', '(99) 99999-9999'],
                                'clientOptions' => ['clearIncomplete' => true]
                        ]) ?>
                        ";

                    break;


                case 'FIELD_CPF':

                    $USE = "use bdb\input\Masked;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(Masked::classname(), [
                                'options'=>[
                                    'maxlength' => 14,
                                    'class' => 'form-control',
                                    'placeholder' => '999.999.999-99',
                                    ],
                                'mask' => '999.999.999-99',
                                'clientOptions' => ['clearIncomplete' => true]
                        ]) ?>
                        ";

                    break;

                case 'FIELD_CNPJ':

                    $USE = "use bdb\input\Masked;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(Masked::classname(), [
                                'options'=>[
                                    'maxlength' => 19,
                                    'class' => 'form-control',
                                    'placeholder' => '999.999.999-99',
                                    ],
                                'mask' => '999.999.999-99',
                                'clientOptions' => ['clearIncomplete' => true]
                        ]) ?>
                        ";

                    break;


                case 'FIELD_ZIP':


                    /*
                    <?= \$form->field(\$model, '{$column->name}')->widget(Masked::classname(), [
                        'options'=>[
                            'maxlength' => 10,
                            'class' => 'form-control',
                            'placeholder' => '99999-999',
                            ],
                        'mask' => '99999-999'
                    ]) ?>
                    */


                    $USE = "use bdb\input\ZipCode;\n";
                    $HTML = "

                        <?= \$form->field(\$model, '{$column->name}')->widget(ZipCode::classname(), [
                            'action' => ['addressSearch?'],
                            'mask' => '99999-999',
                            'clientOptions' => ['clearIncomplete' => true],
                            'fields' => [
                                'location' => '" . strtolower($tableName) . "-address',
                                'district' => '" . strtolower($tableName) . "-neighborhood',
                                'state' => '" . strtolower($tableName) . "-stateid',
                                'city' => '" . strtolower($tableName) . "-cityid'
                            ],
                        ]); ?>

                        ";

                    break;


                case 'FIELD_COLOR_PALLET':

                    $USE = "use kartik\widgets\ColorInput;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(ColorInput::classname(), [
                                'showDefaultPalette' => false,
                                'options'=>['maxlength' => 18, 'placeholder' =>  Yii::t('app', 'FIELD_COLOR_PALLET')],
                                'pluginOptions'=>[
                                    'showPaletteOnly' => true,
                                    'palette' => [array_values(ArrayHelper::map(" . Yii::$app->components['colorPallet'] . "::find()->all(),'id','rgba'))]
                                        ]
                        ]) ?>
                        ";

                    break;


                case 'FIELD_COLOR_PICKER':


                    $USE = "use kartik\widgets\ColorInput;\n";
                    $HTML = "
                        <?= \$form->field(\$model, '{$column->name}')->widget(ColorInput::classname(), [
                                'options'=>['maxlength' => 18, 'placeholder' =>  Yii::t('app', 'FIELD_COLOR_PICKER')],
                                'pluginOptions'=>[
                                    'showButtons' => false,
                                    " . (isset($comment['formOptions']['preferredFormat']) ? "'preferredFormat' => '" . $comment['formOptions']['preferredFormat'] . "'" : "") . "
                                ]
                        ]) ?>
                        ";

                    break;

                default:

                    break;
            }
        }

        return array('HTML' => $HTML, 'USE' => $USE);

    }


    public function getForeignKey($fks)
    {
        $_ENV['output'] = array();
        $_ENV['temp'] = '';
        $_ENV['i'] = 1;

        array_walk_recursive($fks, function ($item, $key) {
            $_ENV['i'] = $_ENV['i'] % 2;

            if ($_ENV['i'] == 1) $_ENV['temp'] = $item;
            else $_ENV['output'][$key] = $_ENV['temp'];

            $_ENV['i']++;

        });

        return $_ENV['output'];
    }


    public static function enumItem($model, $attribute)
    {

        $attr = $attribute;
        preg_match('/\((.*)\)/', $model->tableSchema->columns[$attr]->dbType, $matches);
        foreach (explode(',', $matches[1]) as $value) {
            $value = str_replace("'", null, $value);
            $values[$value] = Yii::t('enumItem', $value);
        }
        asort($values);
        return $values;
    }


    /**
     * @inheritdoc
     */
    public function generate()
    {
        $path = explode('\\', $this->controllerClass);

        $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', $path[0] . '/' . $path[1] . '/base/' . StringHelper::basename($this->controllerClass)) . '.php');
        $controllerFileExtended = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');

        $files = [
            new CodeFile($controllerFile, $this->render('controller.php')),
            new CodeFile($controllerFileExtended, $this->render('controller-extended.php'))
        ];


        if (!empty($this->searchModelClass)) {
            $searchModel = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->searchModelClass, '\\') . '.php'));
            $files[] = new CodeFile($searchModel, $this->render('search.php'));
        }

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/views';
        foreach (scandir($templatePath) as $file) {
            if (empty($this->searchModelClass) && $file === '_search.php') {
                continue;
            }
            if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file"));
            }
        }

        return $files;
    }


    public function getName()
    {
        return 'Bando de Bits CRUD';
    }

    public function getDescription()
    {
        return 'Este gerador gera uma versão estendida do Giiant CRUDs.';
    }

}
