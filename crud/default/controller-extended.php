<?php
	use yii\helpers\StringHelper;

	$controllerClass = StringHelper::basename($generator->controllerClass);
	$modelClass = StringHelper::basename($generator->modelClass);
	$searchModelClass = StringHelper::basename($generator->searchModelClass);
	if ($modelClass === $searchModelClass) {
		$searchModelAlias = $searchModelClass.'Search';
	}

echo "<?php\n";
?>


namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>\base; 


class <?= $controllerClass ?> extends base\<?= $controllerClass ?>

{
}
