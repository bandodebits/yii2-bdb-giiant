<?php

use yii\helpers\StringHelper;

/**
 * This is the template for generating a CRUD controller class file.
 *
 * @var yii\web\View $this
 * @var schmunk42\giiant\crud\Generator $generator
 */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
	$searchModelAlias = $searchModelClass.'Search';
}

$pks = $generator->getTableSchema()->primaryKey;
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();


$model = new $generator->modelClass;
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->getTableSchema()->columnNames;
}

$FKs = $generator->getForeignKey($generator->getTableSchema()->foreignKeys);
$PIVOT = $generator->getPivot();

$HTML = "";
$ACCOUNT = false;
$USE = array();
$IMAGE = array();


$AC_ZIP = false;
$AC_SUB = false;

// TYPEAHEAD
$HTML .= "
public function actionTypeahead(\$q = null, \$w) {
    \$query = new Query;
    \$query->select(\$w)
        ->from('{$model->tableName()}')
        ->where(\$w.' LIKE \"%' . \$q .'%\"')
        ->orderBy(\$w);
    \$command = \$query->createCommand();
    \$data = \$command->queryAll();
    \$out = [];
    foreach (\$data as \$d) {
        \$out[] = ['value' => \$d[\$w]];
    }
    echo json_encode(\$out);
}";


  

foreach ($safeAttributes as $attribute) {

    $column   = $generator->getTableSchema()->columns[$attribute];
    
    $comment = explode('-',$column->comment);

    $tableName = $model->tableName();
    $fkTable = isset($FKs[$column->name]) ? $FKs[$column->name] : "";

    switch ($comment[0]) {
    	case 'FIELD_DEPENDENT_DROPDOWN': 

    		$AC_SUB = "subcat".strtolower($fkTable); 
    		$HTML .= "
			public function actionSubcat".strtolower($fkTable)."() {
			    \$out = [];
			    if (isset(\$_POST['depdrop_parents'])) {
			        \$parents = \$_POST['depdrop_parents'];
			        if (\$parents != null) {
			            \$cat_id = \$parents[0];
			     
			            \$array = \yii\helpers\ArrayHelper::map(\common\models\\".$fkTable."::find()->where(['".$comment[1]."' => \$cat_id])->all(),'id','name');
			            \$_ENV['output'] = [];

			            array_walk(\$array, function (\$item, \$key) {
  			            	\$_ENV['output'][] = ['id' => \$key, 'name' => \$item];
 			            });

			            echo json_encode(['output'=>\$_ENV['output'], 'selected'=>'']);
			            return;
			        }
			    }
			    echo json_encode(['output'=>'', 'selected'=>'']);
			}";

    	break;

     
    	case 'FIELD_ACCOUNT': 

    		$ACCOUNT = true;

    	break;

    	case 'FIELD_ZIP': 

    		$AC_ZIP = true; 

    		$HTML .= "public function actions()
					{
					    return [
					        'addressSearch' => 'bdb\zipcode\ZipcodeAction'
					    ];
					}";

    	break;


    	case 'FIELD_IMAGE': 

    		if(!in_array("use yii\web\UploadedFile;\n", $USE)) $USE[] =  "use yii\web\UploadedFile;\n";

    		$IMAGE[] = $column->name;

    	break;




    	



    	

    	/*
    	case 'FIELD_TYPEAHEAD': 

    		$HTML = "
        	public function actionTypeahead($q = null) {
			    $query = new Query;
			    
			    $query->select('textInput')
			        ->from('Widgets')
			        ->where('textInput LIKE "%' . $q .'%"')
			        ->orderBy('textInput');
			    $command = $query->createCommand();
			    $data = $command->queryAll();
			    $out = [];
			    foreach ($data as $d) {
			        $out[] = ['value' => $d['textInput']];
			    }
			    echo Json::encode($out);
			}";

    	break;
    	*/
    }

}




echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>\base;

use <?= ltrim($generator->modelClass, '\\') ?>;
use <?= ltrim($generator->searchModelClass, '\\') ?><?php if (isset($searchModelAlias)):?> as <?= $searchModelAlias ?><?php endif ?>;
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\db\Query;
use dmstr\bootstrap\Tabs;

<?php 

	if($ACCOUNT) print "use common\models\AuthAccount;";
	print implode(' ', $USE);

 
 ?>

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends \<?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>\basic\Controller
{

	/** BANDO DE BITS **/

	<?= $HTML; ?>







	/** ############################################################################################################# **/




    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' 	=> true,
						'actions'   => ['index', 'view', 'create', 'update', 'delete', 'typeahead' <?php if($AC_SUB) echo ",'".$AC_SUB."'"; if($AC_ZIP) echo ",'addressSearch'"; ?>],
						'roles'     => ['@']
					]
				]
			]
		];
	}

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * Lists all <?= $modelClass ?> models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel  = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>;
		$dataProvider = $searchModel->search($_GET);

		Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single <?= $modelClass ?> model.
	 * <?= implode("\n\t * ", $actionParamComments) . "\n" ?>
     *
	 * @return mixed
	 */
	public function actionView(<?= $actionParams ?>)
	{
        $resolved = \Yii::$app->request->resolve();
        $resolved[1]['_pjax'] = null;
        $url = Url::to(array_merge(['/'.$resolved[0]],$resolved[1]));
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember($url);
        Tabs::rememberActiveState();

        return $this->render('view', [
			'model' => $this->findModelByTsuuid(<?= $actionParams ?>),
		]);
	}

	/**
	 * Creates a new <?= $modelClass ?> model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new <?= $modelClass ?>();
		<?php if($ACCOUNT) print "\$account = new AuthAccount();" ?>

        if (\Yii::$app->request->isAjax && <?php if($ACCOUNT) print "\$account->load(\Yii::\$app->request->post()) &&"; ?>  $model->load(\Yii::$app->request->post())  ) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model<?php if($ACCOUNT) print ", \$account"; ?>);
        }

		try {
			if ($model->load($_POST) <?php if($ACCOUNT) print "&& \$account->load(\$_POST)" ?>) 
            {
            	<?php 
            		if($ACCOUNT) print "
                        \$account->password = strtoupper(hash('sha512', strtoupper(hash('sha512', \$account->password . \Yii::\$app->params['hashPasswordClient'] . \$account->email )) . \Yii::\$app->params['hashPasswordServer']));
                        if(\$account->save()) {
            			\$model->authAccountId = \$account->id;\n"; 

            		if(count($IMAGE) > 0) {
            			print "	// process uploaded image file instance\n";
            			foreach ($IMAGE as $key => $value) {
            				print "				\$image_$value = \$model->uploadImage('{$value}');\n";
            			}
            		}
                        
            	?>

            		if($model->save())
            		{
            			<?php 
            			if(count($IMAGE) > 0) {
	            			print "// upload only if valid uploaded file instance found\n";
	            			foreach ($IMAGE as $key => $value) {
	            				print "				if (\$image_$value !== false) \$image_{$value}->saveAs(\$model->getImageFile('{$value}'));\n";
	            			}
	            		}
	            		?>
                        
                                <?php 
                                    if($generator->isPivot)
                                    {
                                        foreach ($PIVOT as $value) {
                                        print "
                                            //manyMany
                                            \$relational = \\".$model->getRelation($value['manyMany'])->modelClass."::find();
                                            //pega os valores do manyMnay
                                            \$arr = \$_POST['$modelClass']['{$value['manyMany']}'];
                                            //organiza no array somente com as informações necessárias
                                            array_walk(\$arr, function (&\$item, \$key) {
                                                \$item = json_decode(\$item, true);
                                            });
                                            //seleciona os IDs
                                            \$list = array_column (\$arr, 'id');
                                            //faz a busca
                                            \$find = \$relational->having(['id' => \$list])->all();

                                            //percorre o resultado
                                            foreach(\$find as \$key => \$manyMany )
                                            {
                                                //salva a relação
                                                \$model->link('{$value['manyMany']}', \$manyMany, [
                                                    ".($value['select'] != false ? "'".$value['select']."' => \$arr[\$key]['select']," : "")."
                                                    ".($value['spinner'] != false ? "'".$value['spinner']."' => \$arr[\$key]['spinner']," : "")." 
                                                    ".($value['text'] != false ? "'".$value['text']."' => \$arr[\$key]['text']," : "")." 
                                                ]);
                                            }";
                                        }
                                    }
                                ?>

            			return $this->redirect(['view', 'id'=>$model->tsuuid]);
            		}
            	<?php if($ACCOUNT)  print "}"; ?>
                
            }  elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
                <?php if($ACCOUNT) print "\$account->load(\$_GET);" ?>
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
            <?php if($ACCOUNT) print "\$account->addError('_exception', \$msg);" ?>
		}

        <?php if($ACCOUNT)  print "\$account->password = '';"; ?>

        return $this->render('create', [
        'model' => $model,
        <?php if($ACCOUNT) print "'account' => \$account" ?>
        ]);
	}

	/**
	 * Updates an existing <?= $modelClass ?> model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * <?= implode("\n\t * ", $actionParamComments) . "\n" ?>
	 * @return mixed
	 */
	public function actionUpdate(<?= $actionParams ?>)
	{
		$model = $this->findModelByTsuuid(<?= $actionParams ?>);
		<?php 
			if($ACCOUNT) print "\$account = \$model->authAccount;\n \$oldPassword = \$account->password;\n";

			if(count($IMAGE) > 0) {
    			foreach ($IMAGE as $key => $value) {
    				print "\$old_file_$value = \$model->getImageFile('{$value}')\n;
        			\$old_$value = \$model->$value;\n";
    			}
    		}

		?>

        if (\Yii::$app->request->isAjax && <?php if($ACCOUNT) print "\$account->load(\Yii::\$app->request->post()) &&"; ?>  $model->load(\Yii::$app->request->post())  ) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model<?php if($ACCOUNT) print ", \$account"; ?>);
        }

        if ($model->load($_POST) <?php if($ACCOUNT) print "&& \$account->load(\$_POST)" ?>) 
            {
            	<?php 
            		if($ACCOUNT) print "
                        if(trim(\$account->password) != '')
                        \$account->password = strtoupper(hash('sha512', strtoupper(hash('sha512', \$account->password . \Yii::\$app->params['hashPasswordClient'] . \$account->email )) . \Yii::\$app->params['hashPasswordServer']));  
                        else \$account->password = \$oldPassword;

                        if(\$account->save()) {
            			\$model->authAccountId = \$account->id;"; 

            			if(count($IMAGE) > 0) {
	            			print "// process uploaded image file instance\n// revert back if no valid file instance uploaded\n";
	            			foreach ($IMAGE as $key => $value) {
	            				print "				\$image_$value = \$model->uploadImage('{$value}');\n
					            if (\$image_$value === false) \$model->$value = \$old_$value\n;
					            ";
	            			}
	            		}

            		?>
            		if($model->save())
            		{
            			<?php 
            			if(count($IMAGE) > 0) {
	            			print "// upload only if valid uploaded file instance found\n";
	            			foreach ($IMAGE as $key => $value) {
	            				print "				if (\$image_$value !== false) {  if(file_exists(\$old_file_$value)) unlink(\$old_file_$value);  \$image_{$value}->saveAs(\$model->getImageFile('{$value}')); }\n";
	            			}
	            		}
	            		?>
                        
                                <?php 
                                    if($generator->isPivot)
                                    {
                                        foreach ($PIVOT as $value) {
                                        print "
                                            //manyMany
                                            \$relational = \\".$model->getRelation($value['manyMany'])->modelClass."::find();
                                            //pega os valores do manyMnay
                                            \$arr = \$_POST['$modelClass']['{$value['manyMany']}'];
                                            //organiza no array somente com as informações necessárias
                                            array_walk(\$arr, function (&\$item, \$key) {
                                                \$item = json_decode(\$item, true);
                                            });
                                            //seleciona os IDs
                                            \$list = array_column (\$arr, 'id');
                                            //faz a busca
                                            \$find = \$relational->having(['id' => \$list])->all();
                                            \$model->unlinkAll('{$value['manyMany']}', true);

                                            //percorre o resultado
                                            foreach(\$find as \$key => \$manyMany )
                                            {
                                                //salva a relação
                                                \$model->link('{$value['manyMany']}', \$manyMany, [
                                                    ".($value['select'] != false ? "'".$value['select']."' => \$arr[\$key]['select']," : "")."
                                                    ".($value['spinner'] != false ? "'".$value['spinner']."' => \$arr[\$key]['spinner']," : "")." 
                                                    ".($value['text'] != false ? "'".$value['text']."' => \$arr[\$key]['text']," : "")." 
                                                ]);
                                            }";
                                        }
                                    }
                                ?>

            			return $this->redirect(['view', 'id'=>$model->tsuuid]);
            		}
            	<?php if($ACCOUNT)  print "}"; ?>
                
            } else {
            <?php if($ACCOUNT)  print "\$account->password = '';"; ?>
			return $this->render('update', [
				'model' => $model,
				<?php if($ACCOUNT) print "'account' => \$account" ?>
			]);
		}
	}

	/**
	 * Deletes an existing <?= $modelClass ?> model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * <?= implode("\n\t * ", $actionParamComments) . "\n" ?>
	 * @return mixed
	 */
	public function actionDelete(<?= $actionParams ?>)
	{
        $model = $this->findModelByTsuuid($tsuuid);
        <?php
            if($generator->isPivot)
            {
                foreach ($PIVOT as $value) {
                    print "\$model->unlinkAll('{$value['manyMany']}', true);";
                }
            }
        ?>
        
        // validate deletion and on failure process any exception 
        // e.g. display an error message 
        if ($model->delete()) {

        	<?php
        		if(count($IMAGE) > 0) {
    			foreach ($IMAGE as $key => $value) {
    				print "if (!\$model->deleteImage('{$value}')) {
		                Yii::\$app->session->setFlash('error', 'Error deleting image');
		            }";
    			}
    		}
        	?>
            
        }
        return $this->redirect(['index']);
	}

	/**
	 * Finds the <?= $modelClass ?> model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * <?= implode("\n\t * ", $actionParamComments) . "\n" ?>
	 * @return <?= $modelClass ?> the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel(<?= $actionParams ?>)
	{
<?php
if (count($pks) === 1) {
	$condition = '$'.$pks[0];
} else {
	$condition = [];
	foreach ($pks as $pk) {
		$condition[] = "'$pk' => \$$pk";
	}
	$condition = '[' . implode(', ', $condition) . ']';
}
?>
		if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
 



    protected function findModelByTsuuid(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$'.$pks[0];
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findTsuuId(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
