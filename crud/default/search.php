<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);

if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}

$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();
$entity = isset($modelAlias) ? $modelAlias : $modelClass;
$namespace = StringHelper::dirname(ltrim($generator->searchModelClass, '\\'));
$appName = explode('\\', $namespace)[0];
$ext =  ucfirst($appName)  . 'SearchDynamicModel';

echo "<?php\n";
?>

namespace <?= $namespace ?>;

use Yii;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;
use <?= $appName ?>\models\basic\<?= $ext ?>;
use bdb\component\SearchSecuritySpecs as SE;

/**
* <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
*/
class <?= $searchModelClass ?> extends <?= $ext?>
{
    /**
    * @inheritdoc
    */
    public static function modelRules()
    {
        return [
        <?= implode(",\n            ", $rules) ?>,
        ];
    }
 

    public function defaultBaseQuery()
    {
        return <?=$entity?>::find()->root();
    }

    public function defaultSecuritySpecs()
    {
        return [
            <?=$entity?>::class => SE::OPEN
        ];
    }
}
