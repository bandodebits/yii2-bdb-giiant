<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var yii\gii\generators\crud\Generator $generator
 */

$urlParams = $generator->generateUrlParams();
$model = new $generator->modelClass;
	$safeAttributes = $model->safeAttributes();
	if (empty($safeAttributes)) {
	    $safeAttributes = $model->getTableSchema()->columnNames;
	}

	$FKs = $generator->getForeignKey($generator->getTableSchema()->foreignKeys);
	$ACCOUNT = false;

	foreach ($safeAttributes as $attribute) {

	    $column   = $generator->getTableSchema()->columns[$attribute];
	    if($column->comment != NULL) 
	    {
		    $comment = json_decode($column->comment, true);
		    if($comment['formType'] == 'FIELD_ACCOUNT')
	        $ACCOUNT = true;
		}

	}
echo "<?php\n";
?>

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var <?= ltrim($generator->modelClass, '\\') ?> $model
 */

$this->title = '<?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?> ' . $model-><?= $generator->getNameAttribute() ?> . ', ' . <?= $generator->generateString('Edit') ?>;
$this->params['breadcrumbs'][] = ['label' => '<?= Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model-><?= $generator->getNameAttribute() ?>, 'url' => ['view', 'id' => $model->tsuuid]];
$this->params['breadcrumbs'][] = <?= $generator->generateString('Edit') ?>;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass),'-', true) ?>-update">

    <p>
        <?= "<?= " ?>Html::a('<span class="glyphicon glyphicon-eye-open"></span> ' . <?= $generator->generateString('View') ?>, ['view',  'id' => $model->tsuuid], ['class' => 'btn btn-info']) ?>
    </p>

	<?= "<?php " ?>echo $this->render('_form', [
		'model' => $model,
		<?php if($ACCOUNT) echo "'account' => \$account" ?>

	]); ?>

</div>
