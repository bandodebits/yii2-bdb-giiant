<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var yii\gii\generators\crud\Generator $generator
 */


	$model = new $generator->modelClass;
	$safeAttributes = $model->safeAttributes();
	if (empty($safeAttributes)) {
	    $safeAttributes = $model->getTableSchema()->columnNames;
	}

	$FKs = $generator->getForeignKey($generator->getTableSchema()->foreignKeys);
	$ACCOUNT = false;

	foreach ($safeAttributes as $attribute) {

	    $column   = $generator->getTableSchema()->columns[$attribute];

	    if($column->comment != NULL) 
		{
		    $comment = json_decode($column->comment, true);

		    if($comment['formType'] == 'FIELD_ACCOUNT')
	        $ACCOUNT = true;
		}

	}

echo "<?php\n";
?>

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var <?= ltrim($generator->modelClass, '\\') ?> $model
*/

$this->title = <?= $generator->generateString('Create') ?>;
$this->params['breadcrumbs'][] = ['label' => '<?= Inflector::pluralize(
    Inflector::camel2words(StringHelper::basename($generator->modelClass))
) ?>', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-create">

    <p class="pull-left">
        <?= "<?= " ?>Html::a(<?= $generator->generateString('Cancel') ?>, \yii\helpers\Url::previous(), ['class' => 'btn btn-default']) ?>
    </p>
    <div class="clearfix"></div>

    <?= "<?= " ?>$this->render('_form', [
    'model' => $model,
    <?php if($ACCOUNT) echo "'account' => \$account" ?>
    ]); ?>

</div>
