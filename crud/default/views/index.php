<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var schmunk42\giiant\crud\Generator $generator
 */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$model = new $generator->modelClass;

$baseName = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\datecontrol\DateControl;
use bdb\dynagrid\GridViewHelper;

/**
* @var yii\web\View $this
* @var yii\data\ActiveDataProvider $dataProvider
* @var <?= ltrim($generator->searchModelClass, '\\') ?> $searchModel
*/

$modelStateUrlParams = $searchModel->buildModelStateUrlParams();

$column = function ($name, $overrides = []) use ($dataProvider, $searchModel, $modelStateUrlParams) {
    return GridViewHelper::column($name, $dataProvider, $searchModel, $modelStateUrlParams, $overrides);
};

$this->title = <?= "Yii::t('app', 'TABLE_".strtoupper(Inflector::camel2words(StringHelper::basename($generator->modelClass)))."');" ?>
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-title">
    <div class="title-env">
        <h1 class="title"><?= "<?= \$this->title; ?>" ?></h1>
        <p class="description">Administração</p>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="<?= "<?= Url::to(['admin']) ?>" ?>"><i class="fa-home"></i><?= "<?= Yii::t('app', 'Dashdoard') ?>" ?></a>
            </li>
            <li class="active">
                <strong><?= "<?= \$this->title; ?>" ?></strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="<?= $baseName ?>-index">
        <div class="clearfix">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#all" data-toggle="tab" aria-expanded="true"><?= "<?= Yii::t('app', 'Manage') ?>" ?></a>
            </li>
            <!--li class="">
                <a href="#subscribers" data-toggle="tab" aria-expanded="false"><?= "<?= Yii::t('app', 'List') ?>" ?></a>
            </li>
            <li class="">
                <a href="#subscribers" data-toggle="tab" aria-expanded="false"><?= "<?= Yii::t('app', 'Statistic') ?>" ?></a>
            </li-->
            <li class="">
                <?= "<?= Html::a(Yii::t('app', 'New'). \" \" . \$this->title, ['create'], [  'aria-expanded' => 'false']) ?>" ?>
            </li>
        </ul>

        <?= "<?=" ?> DynaGrid::widget([
        'columns'=>[
            [
            'class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
            <?php
            $FKs = $generator->getForeignKey($generator->getTableSchema()->foreignKeys);

            foreach ($generator->getTableSchema()->columns as $column) {

                $filterTypeDef = null;

                if (!empty($column->comment)) {                       
                       $commentMeta = json_decode($column->comment);
                       $filterTypeDef = $commentMeta->filterType;
                }

                if (
                    $filterTypeDef != 'FIELD_HIDDEN' && 
                    $filterTypeDef != 'FILTER_HIDDEN' &&
                    $filterTypeDef != 'FIELD_BLOCK' && 
                    $filterTypeDef != 'FILTER_BLOCK' &&
                    !$column->isPrimaryKey &&
                    substr($column->name, -2) != 'Id'
                    )
                {
                    $field = $column->name;
                    echo "            \$column('$field'),\n";
                }
                
            }
            ?>
            [
                'class'=>'kartik\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index) {
                    $url ='<?= $generator->getControllerID(); ?>/'.$action.'?id='.$model->tsuuid;
                    return $url;
                },
                'noWrap'=>true
            ]
        ],

        //CONFIGURAÇÕES DO GRID
        'storage'=>DynaGrid::TYPE_DB,
        'matchPanelStyle' => false,
        'toggleButtonGrid'=>[
            'label' => '<span class="streamline-setting-gear"></span> '.Yii::t("app", "Grid settings"),
            'class' => 'btn-sm',
            'options' => [
                    'class' => 'btn-sm',
                ],
        ],
    
        'gridOptions'=>[
            'containerOptions' => ['class'=>'panel panel-body'],
            'dataProvider'=>$dataProvider,
            'filterModel'=>$searchModel,
            'headerRowOptions'=>['class'=>'kartik-sheet-style'],
            'filterRowOptions'=>['class'=>'kartik-sheet-style'],
            'pjax' => true,
            'pjaxSettings' => ['loadingCssClass'=>'spinner-grid'],
            'toolbar' =>  [
                ['content'=>
                    Html::a('<i class="streamline-refresh"></i> '.Yii::t('app', 'Reset Grid'), ['/<?= $generator->getControllerID(); ?>'], ['data-pjax'=>0, 'class' => 'btn btn-default btn-sm', 'title'=>Yii::t('app', 'Reset Grid')])
                ],
                ['content'=>
                    '{toggleData}'
                ],
                ['content'=>
                    '{dynagrid}'
                ],
                ['content'=>
                    '{export}'
                ],
            ],
            'resizableColumns'=>true,
            'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m"),
            'showPageSummary' => false,
            'panelHeadingTemplate' => '<span class="pull-left">{summary}</span><span class="pull-right">{toolbar}</span>',
            'panelTemplate' => '<div class="panel {type}">{panelHeading}{items}{panelAfter}{panelFooter}</div>',
            'panel'=>[
                'type' => GridView::TYPE_DEFAULT,
                'class' => 'table-small-font'
            ],
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel'  => Yii::t('app', 'Last')
            ],
            'toggleDataOptions'=>Yii::$app->params['grid']['toggleDataOptions'],
            'exportConfig' => Yii::$app->params['grid']['exportConfig'],
            'export' => Yii::$app->params['grid']['export']
        ],
        'options'=>['id'=>'<?= strtolower ($baseName) ?>'] // a unique identifier is important
        ]);
        ?>
</div>
















