<?php

use yii\helpers\StringHelper;

/**
 * @var yii\web\View $this
 * @var yii\gii\generators\crud\Generator $generator
 */

/** @var \yii\db\ActiveRecord $model */
$model = new $generator->modelClass;
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->getTableSchema()->columnNames;
}

$HTML = "";
$USE = array();
$ACCOUNT = false;

//---------------------------------------------------------------------------------------------
$FKs = $generator->getForeignKey($generator->getTableSchema()->foreignKeys);
 

foreach ($safeAttributes as $attribute) {

    $column   = $generator->getTableSchema()->columns[$attribute]; 
    
    $field = $generator->setField(
         $column, 
         isset($FKs[$column->name]) ? $FKs : NULL,
         isset($FKs[$column->name]) ? 
            @$generator->getRelationByColumn(@$generator->modelClass, $generator->getTableSchema()->columns[$column->name])->modelClass
            //$generator->getModelRelations($generator->modelClass)[$FKs[$column->name]]->modelClass 
         : NULL,
         $model->tableName()
    );

    $HTML .= $field['HTML'];
    if(!in_array($field['USE'], $USE)) $USE[] =  $field['USE'];

    if($column->comment != NULL) 
    {
        $comment = json_decode($column->comment, true);

        if($comment['formType'] == 'FIELD_ACCOUNT')
            $ACCOUNT = true;
    }
    

    /*
    print_r($column->comment); 

    $prepend = $generator->prependActiveField($column, $model);
    $field = $generator->activeField($column, $model);
    $append = $generator->appendActiveField($column, $model);

    if ($prepend) {
        echo "\n\t\t\t<?php " . $prepend . " ?>";
    }
    if ($field) {
        echo "\n\t\t\t<?= " . $field . " ?>";
    }
    if ($append) {
        echo "\n\t\t\t<?php " . $append . " ?>";
    }
    */
} 
//---------------------------------------------------------------------------------------------



echo "<?php\n";
?>

//Base 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

//Externo
use kartik\widgets\ActiveForm;
use \dmstr\bootstrap\Tabs;

//Campos
<?php
    print implode(' ', $USE);
?>


/**
* @var yii\web\View $this
* @var <?= ltrim($generator->modelClass, '\\') ?> $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="<?= \yii\helpers\Inflector::camel2id(StringHelper::basename($generator->modelClass), '-', true) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'], // important
        'id' => '<?= $generator->getControllerID(); ?>-form',
        'enableAjaxValidation' => true,
    ]); ?>
 
    <div class="">
        <?= "<?php " ?>echo $form->errorSummary($model); ?>
        <?php echo "<?php \$this->beginBlock('main'); ?>\n"; ?>

        <?= $HTML; ?>

        <?php
        
        foreach ($generator->getPivot() as $key => $value)
        {
            print "
            <?= \$form->field(\$model, '{$value['manyMany']}')->widget(bdb\input\MultiSelect::classname(), [
                'pivot' => '{$value['pivot']}',
                'spinner' => '{$value['spinner']}',
                'text' => '{$value['text']}',
                'select' => '{$value['select']}'
            ]) ?>";
        }
        
        ?>

        <?php
        if($ACCOUNT) {
          

            print "
                <?= \$form->field(\$account, 'email')->textInput(['maxlength' => 255]) ?>
                ";

            print "
                <?= \$form->field(\$account, 'password')->passwordInput(['maxlength' => 60, 'placeholder' => Yii::t('app', 'FIELD_PASSWORD')]) ?>
                ";

           
        }

         echo "<?php \$this->endBlock(); ?>"; 
        ?>





        <?php
        $label = substr(strrchr($model::className(), "\\"), 1);;

        $items = <<<EOS
                [
                    'label'   => '$label',
                    'content' => \$this->blocks['main'],
                    'active'  => true,
                ],
EOS;
        ?>

        <?=
        "<?=
    Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ $items ]
                 ]
    );
    ?>";
        ?>

        <hr/>

        <?= "<?= " ?>Html::submitButton(
                        '<span class="glyphicon glyphicon-check"></span> ' . ($model->isNewRecord
                                    ? <?= $generator->generateString('Create') ?> : <?= $generator->generateString('Save') ?>),
                        ['class' => 'btn btn-primary']
            );
        ?>

        <?= "<?php " ?>ActiveForm::end(); ?>

    </div>

</div>
