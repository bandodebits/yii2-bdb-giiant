bandodebits giiant
==================
Bando de Bits Gii Code generator

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bandodebits/yii2-bdb-giiant "*"
```

or add

```
"bdb/yii2-bdb-giiant": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \bdb\giiant\AutoloadExample::widget(); ?>```