<?php
/**
 * @link http://www.phundament.com
 * @copyright Copyright (c) 2014 herzog kommunikation GmbH
 * @license http://www.phundament.com/license/
 */

namespace bdb\giiant\model;

use yii\gii\CodeFile;
use yii\helpers\Inflector;
use Yii;
use yii\db\Schema;

/**
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * @author Tobias Munk <schmunk@usrbin.de>
 * @since 0.0.1
 */
class Generator extends \schmunk42\giiant\model\Generator
{


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Bando de Bits Model';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Este gerador gera uma versão estendida do Giiant Model.';
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];
        $relations = $this->generateRelations();
        $db = $this->getDbConnection();
        foreach ($this->getTableNames() as $tableName) {

            $className = $this->generateClassName($tableName);

            $tableSchema = $db->getTableSchema($tableName);
            $params = [
                'tableName' => $tableName,
                'className' => $className,
                'tableSchema' => $tableSchema,
                'labels' => $this->generateLabels($tableSchema),
                'rules' => $this->generateRules($tableSchema),
                'relations' => isset($relations[$className]) ? $relations[$className] : [],
                'teste' => $relations,
                'ns' => $this->ns,
            ];

            $files[] = new CodeFile(
                Yii::getAlias('@' . str_replace('\\', '/', $this->ns)) . '/base/' . $className . '.php',
                $this->render('model.php', $params)
            );

            $modelClassFile = Yii::getAlias('@' . str_replace('\\', '/', $this->ns)) . '/' . $className . '.php';
            if ($this->generateModelClass || !is_file($modelClassFile)) {
                $files[] = new CodeFile(
                    $modelClassFile,
                    $this->render('model-extended.php', $params)
                );
            }
        }
        return $files;
    }


    /**
     * Generates validation rules for the specified table.
     * @param \yii\db\TableSchema $table the table schema
     * @return array the generated validation rules
     */
    public function generateRules($table)
    {
        $types = [];
        $lengths = [];

        foreach ($table->columns as $column) {

            if($column->comment == NULL) {
                if ($column->size > 0) {
                    $lengths[$column->size][] = $column->name;
                } else {
                    $types['string'][] = $column->name;
                }
            }
            else {
            $comment = json_decode($column->comment, true);
            
            if ($comment['formType'] != 'FIELD_BLOCK') {
                    if ($column->autoIncrement) {
                        continue;
                    }

                    if (!$column->allowNull && $column->defaultValue === null && $comment['formType'] != 'FIELD_BLOCK' && $comment['formType'] != 'FIELD_HIDDEN') {
                        $types['required'][] = $column->name;
                    }

                    switch ($column->type) {
                        case Schema::TYPE_SMALLINT:
                        case Schema::TYPE_INTEGER:
                        case Schema::TYPE_BIGINT:
                            $types['integer'][] = $column->name;
                            break;
                        case Schema::TYPE_BOOLEAN:
                            $types['boolean'][] = $column->name;
                            break;
                        case Schema::TYPE_FLOAT:
                        case Schema::TYPE_DOUBLE:
                        case Schema::TYPE_DECIMAL:
                        case Schema::TYPE_MONEY:
                            $types['number'][] = $column->name;
                            break;
                        case Schema::TYPE_DATE:
                        case Schema::TYPE_TIME:
                        case Schema::TYPE_DATETIME:
                        case Schema::TYPE_TIMESTAMP:
                            $types['safe'][] = $column->name;
                            break;
                        default: // strings
                            if ($column->size > 0) {
                                $lengths[$column->size][] = $column->name;
                            } else {
                                $types['string'][] = $column->name;
                            }
                    }
                }
            }
        }

        $rules = [];

        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }

        foreach ($lengths as $length => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], 'string', 'max' => $length]";
        }

        // Unique indexes rules
        try {
            $db = $this->getDbConnection();
            $uniqueIndexes = $db->getSchema()->findUniqueIndexes($table);

            foreach ($uniqueIndexes as $uniqueColumns) {
                // Avoid validating auto incremental columns
                if (!$this->isColumnAutoIncremental($table, $uniqueColumns)) {
                    $attributesCount = count($uniqueColumns);

                    if ($attributesCount == 1) {
                        $rules[] = "[['" . $uniqueColumns[0] . "'], 'unique']";
                    } elseif ($attributesCount > 1) {
                        $labels = array_intersect_key($this->generateLabels($table), array_flip($uniqueColumns));
                        $lastLabel = array_pop($labels);
                        $columnsList = implode("', '", $uniqueColumns);
                        $rules[] = "[['" . $columnsList . "'], 'unique', 'targetAttribute' => ['" . $columnsList . "'], 'message' => 'The combination of " . implode(', ', $labels) . " and " . $lastLabel . " has already been taken.']";
                    }
                }
            }
        } catch (NotSupportedException $e) {
            // doesn't support unique indexes information...do nothing
        }

        return $rules;
    }

    //TODO: fix namespace here
    protected function generateRelations()
    {
        return parent::generateRelations();
    }
}
